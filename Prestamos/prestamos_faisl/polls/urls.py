from django.conf.urls import url
from . import views
#namespace: /polls/
app_name = 'polls'
urlpatterns = [
	url(r'^$', views.index, name='home'),
	url(r'^search', views.search, name='search'),
	url(r'^insertPrestamo', views.insertPrestamo, name='insertPrestamo'),
	url(r'^insert', views.insert, name='insert'),
	url(r'^delete', views.delete, name='delete'),
	url(r'^deleteMaterial', views.deleteMaterial, name="d_material"),
]
