from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .models import Colegios, Profesores, Prestamo, Materiales
import datetime
import time
import pdb

# Create your views here.
# def index(request):
#	     latest_question_list = Colegios.objects.order_by('-id_colegio')[:200]
#	    template = loader.get_template('polls/index.html')
#	    context = {'latest_question_list' : latest_question_list}
#	    return HttpResponse(template.render(context, request))
def index(request):	
	return render(request, 'polls/index.html')

def listado(request):
	try:
		colegios_list = Colegios.objects.all()
	except Colegios.DoesNotExist:
		raise Http404("Colegios Error.")
		return render(request, 'polls/http4.html')
	context = {'latest_question_list' : colegios_list,}
	return render(request, 'polls/listado.html', context)
def formcolegios(request):
	try:
		question = Profesores.objects.all()
	except:
		return render(request, 'polls/http404.html')
	context = {'result' : question}
	return render(request, 'polls/result.html', context)
def search(request):
	try:
		search_text = Colegios.objects.get(nombre= request.POST['nombre_colegio'])
	except:
		return render(request, 'polls/http404.html')
	context = {'result' : search_text}
	return render(request,'polls/result_search_colleges.html', context)

def insert_form(request):
	try:
		colegios_list = Colegios.objects.all()
	except Colegios.DoesNotExist:
		raise Http404("Colegios Error.")
		return render(request, 'polls/http4.html')
	context = {'latest_question_list' : colegios_list,}
	return render(request, 'polls/insert_form.html',context)
def insert(request):
	try:
		new_college = Colegios.objects.create(nombre = request.POST['nombre_colegio'], ciudad = request.POST['ciudad'])
	except:
		return render(request, 'polls/http404.html')
	new_college.save()
	return HttpResponseRedirect('/')
def delete(request):
	try:
		college = Colegios.objects.filter(nombre = request.POST['nombre_colegio'], ciudad = request.POST['ciudad']).delete()
	except:
		return render(request, 'polls/http404.html')
	return HttpResponseRedirect('/')
def prestamos_list(request):
	try:
		prestamos_list = Prestamo.objects.all()
		profesores_list = Profesores.objects.all()
		materiales_list = Materiales.objects.all();
	except Colegios.DoesNotExist:
		raise Http404("Colegios Error.")
		return render(request, 'polls/http4.html')
	context = {'prestamos_list' : prestamos_list,
	'profesores_list' : profesores_list, 'materiales_list' : materiales_list,}
	return render(request, 'polls/list_Prestamos.html', context)
def insert_prestamo_form(request):
	try:
		prestamos_list = Prestamo.objects.all()
		profesores_list = Profesores.objects.all()
		materiales_list = Materiales.objects.all();
	except Colegios.DoesNotExist:
		raise Http404("Colegios Error.")
		return render(request, 'polls/http4.html')
	context = {'prestamos_list' : prestamos_list,
	'profesores_list' : profesores_list, 'materiales_list' : materiales_list,}
	return render(request, 'polls/insert_prestamo_form.html', context)
def insertPrestamo(request):
	try:
		profesor = Profesores.objects.get(nombre = request.POST.get("nombre"), apellido_1 = request.POST.get("apellido_1"))
		material = Materiales.objects.get(nombre = request.POST.get("Material"), numero_identificador = request.POST.get("identificador"))
		if Prestamo.objects.filter(material.nombre, material.id_material).exists():
			return HttpResponse("Ya esiste el prestamo")
		else:
			prestamos = Prestamo(id_profesor = profesor, id_material = material, fecha_inicio = request.POST.get("fecha"), fecha_fin = request.POST.get("fecha_fin"))
	except:
		raise Http404("prestamos insert Error.")
	prestamos.save()
	return HttpResponseRedirect('/')

def listMaterials(request):
	try:
		materials_list = Materiales.objects.all()
	except:
		raise Http404("list material Error.")
	context = {'materialsList' : materials_list}
	return render(request, 'polls/mainMaterials.html', context)
def deleteMaterial(request):
	return HttpResponse("0k")