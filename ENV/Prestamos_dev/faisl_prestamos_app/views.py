from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render, get_object_or_404
from .models import Colegios, Profesores, Prestamo, Materiales, Historial
from django.urls import reverse



# Create your views here.
# def index(request):
#	     latest_question_list = Colegios.objects.order_by('-id_colegio')[:200]
#	    template = loader.get_template('polls/index.html')
#	    context = {'latest_question_list' : latest_question_list}
#	    return HttpResponse(template.render(context, request))

###########################################################################
#Si se accede sin poner /polls/ se redirige.
def index_1(request):
	return HttpResponseRedirect('/faisl_prestamos_app/')

def index(request):
	return render(request, 'faisl_prestamos_app/index.html')
###########################################################################

def listar_centros(request):
	try:
		lista_colegios = Colegios.objects.all()
	except Colegios.DoesNotExist:
		#raise Http404 ("Error al listar colegios.")
		return render(request, 'faisl_prestamos_app/http4.html')
	context = {'lista_colegios' : lista_colegios}
	return render(request, 'faisl_prestamos_app/centros.html', context)

def insertar_centro(request):
	try:
		new_college = Colegios.objects.create(
			nombre = request.POST['nombre_colegio'], 
			ciudad = request.POST['ciudad'])
	except:
		#raise Http404("Error al insertar centro.")
		return render(request, 'faisl_prestamos_app/http404.html')
	new_college.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_centro(request):
	try:
		college = Colegios.objects.filter(
			nombre = request.POST['nombre_colegio']).delete()
	except:
		#raise Http404 ("Error al eliminar centro.")
		return render(request, 'faisl_prestamos_app/http404.html')
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################
def listar_profesores(request):
	try:
		lista_profesores = Profesores.objects.all()
	except:
		raise Http404("Error al listar los profesores.")
		return render(request, 'faisl_prestamos_app/http404.html')
	context = {'lista_profesores' : lista_profesores}
	return render(request, 'faisl_prestamos_app/profesores.html', context)

def insertar_profesor(request):
	try:
		new_profesor = Profesores.objects.create(
			nombre = request.POST['nombre_profesor'], 
			apellido_1 = request.POST['apellido_1'], 
			apellido_2 = request.POST['apellido_2'], 
			telefono = request.POST['telefono'], 
			email = request.POST['email'])
	except:
		raise Http404("Error al insertar profesor.")
		return render(request, 'faisl_prestamos_app/http404.html')
	new_profesor.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_profesor(request):
	try:
		profesor = Profesores.objects.filter(
			nombre = request.POST['nombre_profesor'], 
			apellido_1 = request.POST['apellido_1'], 
			apellido_2 = request.POST['apellido_2']).delete()
	except:
		raise Http404("Error al eliminar profesor.")
		return render(request, 'faisl_prestamos_app/http404.html')
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################

def listar_materiales(request):
	try:
		lista_materiales = Materiales.objects.values('nombre').distinct()
	except:
		#raise Http404("Error al listar los materiales. ")
		return render(request, 'faisl_prestamos_app/http404.html')
	context={'lista_materiales': lista_materiales}
	return render(request, 'faisl_prestamos_app/materiales.html', context)
def insertar_material(request):
	try:
		new_material = Materiales.objects.create(
			nombre = request.POST['nombre'], 
			numero_identificador= request.POST['numero_identificador'])
	except:
		#raise Http404("Error al insertar material.")
		return render(request, 'faisl_prestamos_app/http404.html')
	new_material.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
def eliminar_material(request):
	try:
		material = Materiales.objects.filter(
			nombre = request.POST['nombre'], 
			numero_identificador= request.POST['numero_identificador']).delete()
	except:
		context = {'tipo' : "404"}
		return render(request, 'faisl_prestamos_app/http404.html', context)
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################
def listar_prestamos(request):
	try:
		prestamos_list = Prestamo.objects.filter(fecha_fin = None)
		profesores_list = Profesores.objects.all()
		materiales_list = Materiales.objects.values('nombre').distinct()
		cuenta = Prestamo.objects.filter(fecha_fin = None).count()
	except Colegios.DoesNotExist:
		context = {'tipo' : "404"}
		return render(request, 'faisl_prestamos_app/http404.html', context)
	
	context = {'prestamos_list' : prestamos_list,
	'profesores_list' : profesores_list, 
	'materiales_list' : materiales_list,
	'cuenta' : cuenta,}
	cambiarDisponible(request)
	return render(request, 'faisl_prestamos_app/prestamos.html', context)
def insertar_prestamo(request):
	try:
		profesor = Profesores.objects.get(nombre = request.POST.get("nombre"), 
			apellido_1 = request.POST.get("apellido_1"))
		material = Materiales.objects.get(nombre = request.POST.get("Material"), 
			numero_identificador = request.POST.get("identificador"))
		prestamos = Prestamo.objects.create(id_profesor = profesor, 
		id_material = material, 
		fecha_inicio = request.POST.get("fecha"), 
		otros = request.POST.get("otros"))
	except Exception as e:
		print e
	prestamos.save()
	cambiarDisponible(request)
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
def eliminar_prestamo(request):
	try:
		profesor = Profesores.objects.get(nombre = request.POST.get("nombre"), 
			apellido_1 = request.POST.get("apellido_1"))
		material = Materiales.objects.get(nombre = request.POST.get("Material"), 
			numero_identificador = request.POST.get("identificador"))
		prestamos = Prestamo.objects.filter(id_profesor = profesor, 
			id_material = material)
		prestamos.update(fecha_fin = request.POST.get("fecha_fin"))
		prestamos.values("fecha_inicio")
	except Exception as e:
		print e
	cambiarDisponible(request)
	#prestamos.delete()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
def cambiarDisponible(request):
	try:
		prestamo = Prestamo.objects.values_list('fecha_fin', flat=True)
		prestamos = prestamo.values('id_material')
		material = Materiales.objects.filter(id_material__in = prestamos)
		for var in prestamo:
			if var == None:
				material.update(disponible = False)
			else:
				material.update(disponible = True)
	except Exception as e:
		print e
	finally:
		return HttpResponse("0k")
#########################################################################
def refresh(request):
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
def history(request):
	try:
		historial = Prestamo.objects.exclude(fecha_fin = None)
	except:
		context = {'tipo' : "NotFound"}
		return render(request, 'faisl_prestamos_app/http404.html', context)
	context = {'lista_historial' : historial}
	return render(request, 'faisl_prestamos_app/historial.html', context)
def insertar_en_Historial(request):
	try:
		prestamo = Prestamo.objects.filter(fecha_fin__in != None)
		historico = Historial(prestamo)
	except Exception as e:
		print e
	finally:
		return render(request, 'faisl_prestamos_app/historial.html')