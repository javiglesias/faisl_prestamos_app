from __future__ import unicode_literals

from django.apps import AppConfig


class FaislPrestamosAppConfig(AppConfig):
    name = 'faisl_prestamos_app'
