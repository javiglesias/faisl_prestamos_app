from django.conf.urls import url
from . import views
#namespace: /polls/
app_name = 'polls'
urlpatterns = [
	url(r'^$', views.index, name='home_polls'),
	url(r'^centros', views.listar_centros, name='centros'),
	url(r'^insertar_centro', views.insertar_centro, name='insertar_centro'),
	url(r'^eliminar_centro', views.eliminar_centro, name='eliminar_centro'),
	url(r'^profesores', views.listar_profesores, name='profesores'),
	url(r'^insertar_profesor', views.insertar_profesor, name='insertar_profesor'),
	url(r'^eliminar_profesor', views.eliminar_profesor, name='eliminar_profesor'),
	url(r'^materiales', views.listar_materiales, name='materiales'),
	url(r'^insertar_material', views.insertar_material, name='insertar_material'),
	url(r'^eliminar_material', views.eliminar_material, name='eliminar_material'),
	url(r'^prestamos', views.listar_prestamos, name='prestamos'),
	url(r'^insertar_prestamo', views.insertar_prestamo, name='insertar_prestamo'),
	url(r'^eliminar_prestamo', views.eliminar_prestamo, name='eliminar_prestamo'),
	url(r'^refresh', views.refresh, name='refresh'),
	url(r'^historial', views.history, name='historial'),
]

