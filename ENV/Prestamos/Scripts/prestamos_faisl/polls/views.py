from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from .models import Colegios, Profesores, Prestamo, Materiales

# Create your views here.
# def index(request):
#	     latest_question_list = Colegios.objects.order_by('-id_colegio')[:200]
#	    template = loader.get_template('polls/index.html')
#	    context = {'latest_question_list' : latest_question_list}
#	    return HttpResponse(template.render(context, request))

###########################################################################
#Si se accede sin poner /polls/ se redirige.
def index_1(request):
	return HttpResponseRedirect('/polls/')

def index(request):
	return render(request, 'polls/index.html')
###########################################################################

def listar_centros(request):
	try:
		lista_colegios = Colegios.objects.all()
	except Colegios.DoesNotExist:
		#raise Http404 ("Error al listar colegios.")
		return render(request, 'polls/http4.html')
	context = {'lista_colegios' : lista_colegios}
	return render(request, 'polls/centros.html', context)

def insertar_centro(request):
	try:
		new_college = Colegios.objects.create(nombre = request.POST['nombre_colegio'], ciudad = request.POST['ciudad'])
	except:
		#raise Http404("Error al insertar centro.")
		return render(request, 'polls/http404.html')
	new_college.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_centro(request):
	try:
		college = Colegios.objects.filter(nombre = request.POST['nombre_colegio']).delete()
	except:
		#raise Http404 ("Error al eliminar centro.")
		return render(request, 'polls/http404.html')
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################
def listar_profesores(request):
	try:
		lista_profesores = Profesores.objects.all()
	except:
		raise Http404("Error al listar los profesores.")
		return render(request, 'polls/http404.html')
	context = {'lista_profesores' : lista_profesores}
	return render(request, 'polls/profesores.html', context)

def insertar_profesor(request):
	try:
		new_profesor = Profesores.objects.create(nombre = request.POST['nombre_profesor'], apellido_1 = request.POST['apellido_1'], apellido_2 = request.POST['apellido_2'], telefono = request.POST['telefono'], email = request.POST['email'])
	except:
		raise Http404("Error al insertar profesor.")
		return render(request, 'polls/http404.html')
	new_profesor.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_profesor(request):
	try:
		profesor = Profesores.objects.filter(nombre = request.POST['nombre_profesor'], apellido_1 = request.POST['apellido_1'], apellido_2 = request.POST['apellido_2']).delete()
	except:
		raise Http404("Error al eliminar profesor.")
		return render(request, 'polls/http404.html')
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################

def listar_materiales(request):
	try:
		lista_materiales = Materiales.objects.all()
	except:
		#raise Http404("Error al listar los materiales. ")
		return render(request, 'polls/http404.html')
	context={'lista_materiales': lista_materiales}
	return render(request, 'polls/materiales.html', context)

def insertar_material(request):
	try:
		new_material = Materiales.objects.create(nombre = request.POST['nombre'], numero_identificador= request.POST['numero_identificador'])
	except:
		#raise Http404("Error al insertar material.")
		return render(request, 'polls/http404.html')
	new_material.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_material(request):
	try:
		material = Materiales.objects.filter(nombre = request.POST['nombre'], numero_identificador= request.POST['numero_identificador']).delete()
	except:
		#raise Http404("Error al eliminar material.")
		return render(request, 'polls/http404.html')
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

###########################################################################

def listar_prestamos(request):
	try:
		prestamos_list = Prestamo.objects.all()
		profesores_list = Profesores.objects.all()
		materiales_list = Materiales.objects.all();
	except Colegios.DoesNotExist:
		raise Http404("Error al listar prestamos.")
		return render(request, 'polls/http4.html')
	context = {'prestamos_list' : prestamos_list,'profesores_list' : profesores_list, 'materiales_list' : materiales_list,}
	return render(request, 'polls/prestamos.html', context)

def insertar_prestamo(request):
	try:
		profesor = Profesores.objects.get(nombre = request.POST.get("nombre"), apellido_1 = request.POST.get("apellido_1"))
		material = Materiales.objects.get(nombre = request.POST.get("Material"), numero_identificador = request.POST.get("identificador"))
		prestamos = Prestamo(id_profesor = profesor, id_material = material, fecha_inicio = request.POST.get("fecha"), fecha_fin = request.POST.get("fecha_fin"))
	except:
		raise Http404("Error al insertar prestamo.")
		return render(request, 'polls/http404.html')
	prestamos = Prestamo(id_profesor = profesor, id_material = material, fecha_inicio = request.POST.get("fecha"))
	prestamos.save()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def eliminar_prestamo(request):
	try:
		profesor = Profesores.objects.get(nombre = request.POST.get("nombre"), apellido_1 = request.POST.get("apellido_1"))
		material = Materiales.objects.get(nombre = request.POST.get("Material"), numero_identificador = request.POST.get("identificador"))
		prestamos = Prestamo.objects.get(id_profesor = profesor, id_material = material)
	except:
		raise Http404("Error al eliminar prestamo.")
		return render(request, 'polls/http404.html')
	prestamos.delete()
	return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))