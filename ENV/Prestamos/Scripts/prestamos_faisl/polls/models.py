# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Colegios(models.Model):
    id_colegio = models.AutoField(db_column='ID_colegio', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=100)  # Field name made lowercase.
    ciudad = models.CharField(db_column='Ciudad', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'colegios'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Materiales(models.Model):
    id_material = models.AutoField(db_column='ID_MATERIAL', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    numero_identificador = models.IntegerField(db_column='NUMERO_IDENTIFICADOR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'materiales'


class Prestamo(models.Model):
    id_prestamos = models.AutoField(db_column='ID_PRESTAMOS', primary_key=True)  # Field name made lowercase.
    id_profesor = models.ForeignKey('Profesores', models.DO_NOTHING, db_column='ID_PROFESOR')  # Field name made lowercase.
    id_material = models.ForeignKey(Materiales, models.DO_NOTHING, db_column='ID_MATERIAL')  # Field name made lowercase.
    fecha_inicio = models.CharField(db_column='FECHA_INICIO', max_length=15)  # Field name made lowercase.
    fecha_fin = models.CharField(db_column='FECHA_FIN', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prestamo'


class Profesores(models.Model):
    id_profesor = models.AutoField(db_column='ID_PROFESOR', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    apellido_1 = models.CharField(db_column='APELLIDO_1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    apellido_2 = models.CharField(db_column='APELLIDO_2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    telefono = models.IntegerField(db_column='TELEFONO', blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'profesores'


class ProfesoresColegios(models.Model):
    identificacion_profesor = models.ForeignKey(Profesores, models.DO_NOTHING, db_column='Identificacion_profesor')  # Field name made lowercase.
    identificacion_colegio = models.ForeignKey(Colegios, models.DO_NOTHING, db_column='Identificacion_colegio')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'profesores_colegios'
        unique_together = (('identificacion_profesor', 'identificacion_colegio'),)
